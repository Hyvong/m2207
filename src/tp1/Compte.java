package tp1;

public class Compte
{
	//Attributs
	
	private int numero;
	private double solde, decouvert;
	
	//Constructeurs
	
	public Compte (int num)
	{
		this.numero = num;
		this.solde = 0;
		this.decouvert = 0;
	}
	
	//Accesseurs
	
	public int getNumero() { return this.numero; }
	public double getSolde() { return this.solde; }
	public double getDecouvert() { return this.decouvert; }
	public void setDecouvert(double montant) { this.decouvert = montant; }
	
	//Méthodes
	
	public void afficherSolde()
	{
		System.out.println("Solde : "+this.getSolde());
	}
	
	public void depot(double montant)
	{
		this.solde+=montant;
	}
	
	public String retrait(double montant)
	{
		double montantMax = this.solde + this.decouvert;
		if(montant > montantMax)
		{
			return "Retrait refusé !";
		}
		else
		{
			this.solde -= montant;
			return "Retrait effectué";
		}
	}
	
	String virer(Compte destinataire, double montant)
	{
		double montantMax = this.solde + this.decouvert;
		if (montant > montantMax)
		{
			return "Virement refusé !";
		}
		else
		{
			this.solde -= montant;
			destinataire.depot(montant);
			return "Virement effectué";
		}
	}
}

