package tp1;

public class TestClient
{
	public static void main(String[] args)
	{
		Compte compteClient1 = new Compte(1001);
		Client client1 = new Client("Dupont", "Philippe", compteClient1);
				
		System.out.println("Identité du client1 : " + client1.getNom() +" "+ client1.getPrenom());
		client1.afficherSolde();
		
		//Exercice 4.2
		Compte c3 = new Compte(3);
		ClientMultiComptes clientMulti1 = new ClientMultiComptes("Chamonier", "Bertrand", c3);
		
		//On crée les compts à ajouter à notre client multicomptes
		Compte c4 = new Compte(4);
		Compte c5 = new Compte(5);
		
		//On attribue les comptes 4 et 5 au client multicomptes
		clientMulti1.ajouterCompte(c4);
		clientMulti1.ajouterCompte(c5);
		
		//On dépose sur les comptes
		c3.depot(1405);
		c4.depot(250);
		c5.depot(1200);
		
		//on calcule la somme des soldes de tous les comptes
		System.out.println("Somme du solde de tous les comptes : " +clientMulti1.getSolde());
		
	}
}
