package tp1;

public class Client
{
	//Attributs
	
	private String nom, prenom;
	private Compte compteCourant;
	
	//Constructeurs
	
	public Client(String n, String p, Compte compte)
	{
		this.compteCourant = compte;
		this.nom = n;
		this.prenom = p;
	}
	
	//Accesseurs
	
	public String getNom() { return this.nom; }
	public String getPrenom() { return this.prenom; }
	public double getSolde() { return this.compteCourant.getSolde(); }
	//Méthodes
	
	public void afficherSolde()
	{
		System.out.println("Solde: "+ this.compteCourant.getSolde());
	}
}
