package tp5;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MonAppliGraphique extends JFrame implements ActionListener
{
	//Attributs
	//JButton b1, b2, b3, b4, b5;
	
	/*
	JButton bClic;
	JLabel labelClic;
	private int compteurClic;
	*/
	
	JButton bVerif;
	JLabel proposition, reponse;
	JTextField choix;
	private int aleat, cptCoup;
	
	
	//Constructeurs
	public MonAppliGraphique()
	{
		/*
		
		//Création
		super();
		this.setTitle("Ma première application"); //Titre de la fenêtre
		this.setSize(400, 200); //Espace de l'écran occupé par la fenêtre
		this.setLocation(20, 20); //Position par rapport au bord de l'écran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On peut fermer l'application en fermant la fenêtre
		System.out.println("La fenêtre est créée !");
		
		//Initialisation
		b1 = new JButton("Bouton 1");
		b2 = new JButton("Bouton 2");
		b3 = new JButton("Bouton 3");
		b4 = new JButton("Bouton 4");
		b5 = new JButton("Bouton 5");

		Container panneau = getContentPane();
		
		//Exercice 2.2
		//Donne une taille fixe au bouton et le centre horizontalement quelque soit les dimensions de la fenêtre.
		panneau.setLayout(new GridLayout(2,3));
		
		panneau.add(b1); // Boutons répartis en 2 lignes et 3 colonnes.
		panneau.add(b2);
		panneau.add(b3);
		panneau.add(b4);
		panneau.add(b5);
		
		this.setVisible(true); //La fenêtre est visible
		
		*/
		
		/*
		super();
		this.setTitle("CompteurDeClic");
		this.setSize(200,100);
		this.setLocation(100, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fenêtre est créée");
		
		bClic = new JButton("Click !");
		labelClic = new JLabel("Vous avez cliqué 0 fois");
		
		Container panneau = getContentPane();
		panneau.setLayout(new FlowLayout());
		
		panneau.add(bClic);
		panneau.add(labelClic);
		
		bClic.addActionListener(this);
		
		this.setVisible(true);
		*/

		super();
		this.setTitle("Plus cher ou moins cher ?");
		this.setSize(600, 150);
		this.setLocation(300, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		bVerif = new JButton("Vérifie !");
		proposition = new JLabel("Votre proposition :", SwingConstants.CENTER);
		reponse = new JLabel("Devinez !", SwingConstants.CENTER);
		choix = new JTextField();
		//Choix d'un nombre aléatoire entre 0 et 100
		this.aleat = 0 + (int)(Math.random() * ((100 - 0) + 1));
		this.cptCoup = 8; //8 coups permis par partie
		
		Container panneau = getContentPane();
		panneau.setLayout(new GridLayout(2, 2));
		
		//Ordre d'affichage
		panneau.add(proposition);
		panneau.add(choix);
		panneau.add(bVerif);
		panneau.add(reponse);

		System.out.println("Nombre aléatoire: "+ this.aleat);
		
		//Actions
		bVerif.addActionListener(this);
		
		this.setVisible(true);
	}
	
	//Méthodes
	public void actionPerformed(ActionEvent e)
	{
		//System.out.println("Une action a été détectée !");
		//compteurClic++;
		//labelClic.setText("Vous avez cliqué "+ compteurClic +" fois"); //Mise à jour du nombre de clics
		
		if (this.cptCoup >= 1 )
		{
			//System.out.println("Choix: "+ choix.getText()); //Affiche le contenu du champ texte lorsqu'on appuie sur le bouton.
			int nbChoix = Integer.parseInt(choix.getText());
			if(nbChoix > this.aleat)
			{
				reponse.setText("Moins cher !");
				this.cptCoup--; //Coup décompté
			}
			else if(nbChoix < this.aleat)
			{
				reponse.setText("Plus cher !");
				this.cptCoup--; //Coup décompté
			}
			else //Cas de victoire
			{
				reponse.setText("Victoire ! Devinez à nouveau !");
				this.cptCoup--; //Coup décompté
				
				System.out.println("Partie remportée en "+ (8-this.cptCoup) +" coups !");
				this.init();	//Partie suivante
			}
			

			if(this.cptCoup == 0) {//Cas de défaite : 8 coups épuisés
				reponse.setText("Perdu ! Devinez à nouveau !");
				this.init();
			}
			System.out.println("Coups restants :" + this.cptCoup);
		}
		
	}
	
	public void init() //Réinitialisation pour la partie suivante
	{
		choix.setText(""); //On efface le champ de saisie pour laisser le joueur taper un autre nombre
		//De plus, cela permet d'effacer toute trace visuelle de la partie d'avant et d'inciter à rejouer.
		
		//Choix d'un nombre aléatoire entre 0 et 100
		this.aleat = 0 + (int)(Math.random() * ((100 - 0) + 1));
		System.out.println("Nombre aléatoire: "+ this.aleat);
		
		this.cptCoup = 8; //Nouvelle manche, nouvelles chances
	}
	
	//Méthode main
	public static void main(String[] args)
	{
		MonAppliGraphique PlusOuMoinsCher = new MonAppliGraphique();
	}
}
