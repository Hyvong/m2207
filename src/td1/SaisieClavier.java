package td1;

import java.util.*;

public class SaisieClavier
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int entier;
		double reel;
		
		System.out.println("Saisissez un prénom : ");
		String str = sc.nextLine();
		System.out.println("Saisissez un entier : ");
		entier = sc.nextInt();
		System.out.println("Saisissez un réel : ");
		reel = sc.nextDouble();

		
		double somme = reel + (double)entier;
		
		System.out.println("Bonjour "+str+", la somme de "+entier+" et "+reel+" est "+somme);
	}

}
