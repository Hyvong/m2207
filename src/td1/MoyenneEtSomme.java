package td1;

public class MoyenneEtSomme
{
	public static void main(String[] args)
	{
		int somme = 0;
		double moyenne = 0;
		
		for (int i = 1; i <= 100; i++)
		{
			somme += i; // Calcul de la somme de tous les entiers de 1 à 100.
		}
		System.out.println("La somme est de : "+ somme);
		
		moyenne = (double)somme / 100;
		System.out.print("La moyenne est de : " + moyenne);
	}
}
