package td1;

public class SanteDeBob{

	public static void main(String[] args)
	{
		String nom = "Bob";
		int ageBob = 18;

		float poidsBob = 58;
		int ageRetraiteBob = 65;
		System.out.println(nom + " a " + ageBob + " ans, pèse "+ poidsBob+" kgs"
				+ " et il lui reste "+ (ageRetraiteBob - ageBob) +" année(s) avant la retraite.");
		
		float poidsLimiteBob = 70;
		System.out.println(nom + " dépassera "+ poidsLimiteBob +" kgs à " + ageFromKg(ageBob, poidsBob, poidsLimiteBob) + " ans.");
		
		int ageVieilliBob = 40;
		double poidsVieilliBob = kgFromAge(ageVieilliBob);
		System.out.println("A "+ageVieilliBob+" ans, "+nom+" pèsera "+poidsVieilliBob+" kgs.");
		
		double indiceMasseCorporelleBob = imc(poidsVieilliBob);
		System.out.print("A "+ageVieilliBob+" ans, "+nom+" aura un IMC de "+indiceMasseCorporelleBob+ ". ");
		if (indiceMasseCorporelleBob > 25)
		{
			System.out.println(nom+" est en surpoids !");
		}
		
		int objectifBob = 23;
		int kilometres = pertePoids(indiceMasseCorporelleBob, objectifBob, poidsVieilliBob);
		System.out.println(nom+" devra parcourir "+kilometres+" km pour arriver à un IMC de "+ objectifBob+". ");
	}
	
	// Calcule l'age de Bob lorsqu'il dépassera les 70 kgs
	public static int ageFromKg(int age, float poids, float poidsLimite)
	{
		while ( poids < poidsLimite )
		{
			poids+=1.2;
			age++;
		}
		return age;
	}
	
	// Calcule le poids de Bob en fonction de son âge.
	public static double kgFromAge(int ageChoisi)
	{
		double poids = 58 + 1.2 * (ageChoisi - 18);
		return poids;
	}
	
	// Calcule l'indice de masse corporelle de Bob
	public static double imc(double poids)
	{
		double tailleBob = 1.82;

		double indiceMC = poids / (tailleBob * tailleBob);
		return indiceMC;
	}
	
	public static int pertePoids(double imc, float objectif, double poids)
	{
		int km = 0;
		while( imc > objectif)
		{
			km++;
			poids-=0.024;
			imc = imc(poids);
		}
		return km;
	}
	
}
