package tp4;

import java.util.Scanner;

public class CombatMultiPokemon
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		Pokemon tabPokemon[][]; //On stocke les pokemons dans un tableau de deux lignes et deux colonnes (deux pokemons pour chaque joueur).
		tabPokemon = new Pokemon[2][2];
		
		//Nommage des pokemons
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				//Utilisation de la position dans le tableau pour identifier le joueur et le pokemon à nommer.
				System.out.println("J" + (i+1) + ": Pokemon " + (j+1) + ":"); 
				tabPokemon[i][j] =  new Pokemon(scan.nextLine());
			}
		}
	
		//Présentation des pokemons
		//On utilise des boucles for imbriquées pour parcourir l'ensemble du tableau en 2 dimensions
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				tabPokemon[i][j].sePresenter();
			}
		}
		
		int cptRound = 0; //Compteur de round
		
		//On ne sort pas de la boucle tant que les Pokemons ont la force de se battre
		while ( (tabPokemon[0][0].isAlive() || tabPokemon[0][1].isAlive()) && (tabPokemon[1][0].isAlive() || tabPokemon[1][1].isAlive()))
		{
			System.out.println("=====================");
			
			cptRound++;
			System.out.println("Round "+cptRound);
			
			//Affichage situation
			//Même système que pour la présentation initiale
			System.out.println("Etat des combattants:");
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					System.out.println(tabPokemon[i][j].getNom() + " (en)" + tabPokemon[i][j].getEnergie()
							+ " (atk)"+tabPokemon[i][j].getPuissance() );
					
				}
			}
			
			//Phase choix
			//Utilisation des boucles for imbriquées avec choix de l'adversaire à attaquer
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					if (tabPokemon[i][j].isAlive()) {
						System.out.println(tabPokemon[i][j].getNom() + ": Attaquer(0) ou Manger(1) ?");
						int choix = scan.nextInt();
						
						
							//Scenario d'attaque
							if (choix == 0) {
								System.out.println("Attaquer qui ?");
								Pokemon cible = null;
								int choixCible = 0;
								
								if (i == 0) { //Si on se situe dans la première ligne du tableau (joueur 1)
									for (int k = 0; k < 2; k++)
									{
										if(tabPokemon[i+1][k].isAlive()) //Si le pokemon adversaire sélectionné est vivant...
										{
											//...on l'affiche comme choix de cible potentielle
											System.out.println(tabPokemon[i+1][k].getNom() + "("+ k +")"); 
										}
									}
									choixCible = scan.nextInt(); //Entrée de la cible au clavier (0 ou 1)
									cible = tabPokemon[i+1][choixCible]; //On définit l'adversaire en avec le choix précédent
								}
								else //Si on se situe dans la seconde ligne du tableau (joueur 2)
								{
									for (int k = 0; k < 2; k++)
									{
										if(tabPokemon[i-1][k].isAlive())
										{
											System.out.println(tabPokemon[i-1][k].getNom() + "("+ k +")");
										}
									}
									choixCible = scan.nextInt(); //Entrée de la cible au clavier (0 ou 1)
									cible = tabPokemon[i-1][choixCible]; //On définit l'adversaire en avec le choix précédent

								}
							
								tabPokemon[i][j].attaquer(cible);
							}
							//Scenario de régénération par IAN (Ingurgitation d'Aliments Nutritifs)
							else if (choix == 1) {
								tabPokemon[i][j].manger();
							}
							else {
								//Si le choix n'est pas compris, on passe le tour du pokemon
								System.out.println( tabPokemon[i][j].getNom() + " ne fait rien ...");
							}
					}
				}
			}
		
		}
		System.out.println("=====================");
		
		//Dans ce mode, il n'y a pas de match nul car les attaques s'appliquent directement après le choix
		
		if (tabPokemon[0][1].isAlive() || tabPokemon[0][1].isAlive()) //Si J1 a encore un pokemon debout
		{
			System.out.println("Le J1 a gagné en "+ cptRound + " rounds !");
		}
		else //...sinon c'est l'autre équipe qui possède un Pokemon vivant et c'est celle-ci qui remporte la victoire
		{
			System.out.println("Le J2 a gagné en "+ cptRound + " rounds !");
		}
	}
}
