package tp4;

public class TestPokemon
{
	public static void main(String[] args)
	{
		Pokemon pikinutile = new Pokemon("Magicarpe");

		pikinutile.sePresenter(); //On relance le programme plusieurs fois pour vérifier que les valeurs changent correctement (energie < maxEnergie
		
		int cpt = 0; //Compteur de cycles de vie
		while (pikinutile.getEnergie() > 0) //Les cycles se succèdent tant que le Pokemon est en vie
		{
			pikinutile.manger();
			pikinutile.vivre();
			cpt++;
		}
		System.out.println(pikinutile.getNom() + " a vécu "+ cpt +" cycles.");
		
	}

}
