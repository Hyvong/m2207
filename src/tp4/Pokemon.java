package tp4;

public class Pokemon
{
	//Attributs
	private int energie, maxEnergie, puissance;
	private String nom;
	
	//Constructeurs
	public Pokemon(String n)
	{
		int nbAleat; //Le nombre aléatoire qu'on va calculer et attribuer aux attributs d'énergie
		
		this.nom = n;
		
		nbAleat = 50 + (int)(Math.random() * ((90 - 50 ) + 1));	//Le nombre est choisi aléatoirement entre 50 et 90
		this.maxEnergie = nbAleat;
		
		nbAleat = 30 + (int)(Math.random() * ((maxEnergie - 30 ) + 1));	//Le nombre est choisi aléatoirement entre 30 et maxEnergie
		this.energie = nbAleat;
		
		nbAleat = 3 + (int)(Math.random() * ((10 - 3 ) + 1));	//Le nombre est choisi aléatoirement entre 3 et 10
		this.puissance = nbAleat;

	}
	
	//Accesseurs
	public int getEnergie() { return this.energie; }
	public String getNom() { return this.nom; }
	public int getMaxEnergie() { return this.maxEnergie; }
	public int getPuissance() { return this.puissance; }
	
	//Méthodes
	public void sePresenter()
	{
		//Phrase de présentation d'un Pokemon
		System.out.println("Je suis "+ this.getNom() + ", j'ai "
							+ this.getEnergie() + " points d'énergie (" + this.getMaxEnergie() + " max) et une puissance de "+this.getPuissance());
	}
	
	public void manger() //Miam
	{
		if(this.energie > 0) //Si le Pokemon a encore de l'énergie alors il va pouvoir récupérer entre 10 et 30 points d'énergie en mangeant
		{
			int nbAleat = 10 + (int)(Math.random() * ((30 - 10) + 1));
			this.energie += nbAleat;
			if (this.energie > this.maxEnergie)
			{
				this.energie = this.maxEnergie; //On limite l'énergie acquise grâce à la valeur de maxEnergie
			}
		}
		else //Ici le Pokemon ne peut pas récuperer d'énergie
		{
			System.out.println(this.getNom() +" n'a plus la force de se nourrir...");
		}
	}
	
	public void vivre() //Fait perdre 'naturellement' de l'énergie au Pokemon
	{
		int nbAleat = 20 + (int)(Math.random() * ((40 - 20) + 1));
		this.energie -= nbAleat;
		if (this.energie < 0)
		{
			this.energie = 0; //Si l'énergie descend dans le négatif, on la remet à zéro
		}
	}
	
	public boolean isAlive() //Vérifie si le Pokemon est en vie
	{
		if (this.energie > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean enFurie() //Activation de la puissance cachée des Pokemons affaiblis
	{
		if(this.energie < 0.25*this.maxEnergie)
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	public void perdreEnergie(int perte) //Permet de retirer de l'énergie à un Pokemon touché(coulé) par une attaque
	{
		if (this.enFurie()) //Applique la perte de 1.5x plus d'énergie si le Pokemon touché(recoulé) est en furie.
		{
			this.energie -= 1.5*perte;
		}
		else
		{
			this.energie -= perte;
		}
		
		if (this.energie < 0)
		{
			this.energie = 0;
		}
	}
	
	public void attaquer(Pokemon adversaire) //Permet d'asséner une attaque à l'adversaire passé en argument
	{
		if (this.enFurie())//Applique une attaque 2x plus puissante si le Pokemon attaquant est en furie.
		{
			adversaire.perdreEnergie(this.puissance*2);
		}
		else
		{
			adversaire.perdreEnergie(this.puissance);
		}

		
		this.puissance -= (int)(Math.random() * 2);	//L'expression de l'aléatoire est simplifiée car les bornes sont 0 et 1.
		if (this.puissance < 1)
		{
			this.puissance = 1;
		}
	}
}
