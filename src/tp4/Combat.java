package tp4;

public class Combat
{
	public static void main(String[] args)
	{
		Pokemon pikaMegaBrutal = new Pokemon("Tyranocif");
		Pokemon pikaLegende = new Pokemon("Elector");
		
		pikaMegaBrutal.sePresenter();
		pikaLegende.sePresenter();
		
		System.out.println("======================================");
		
		int cptRound = 0; //Compteur de round
		
		//On ne sort pas de la boucle tant que les Pokemonq ont la force de se battre
		while (pikaLegende.isAlive() && pikaMegaBrutal.isAlive())
		{
			cptRound++;
			
			//Phase d'attaque
			pikaMegaBrutal.attaquer(pikaLegende);
			pikaLegende.attaquer(pikaMegaBrutal);
			
			//Récapitulatif du round
			System.out.println("Round "+cptRound+ " - " + pikaMegaBrutal.getNom() +": (en)"+ pikaMegaBrutal.getEnergie() + " (atk) "+pikaMegaBrutal.getPuissance()
														+" "+ pikaLegende.getNom() + ": (en)"+ pikaLegende.getEnergie() + " (atk) " + pikaLegende.getPuissance());			
		}
		
		//Affichage du vainqueur
		if( (pikaMegaBrutal.isAlive() || pikaLegende.isAlive()) == false) // Cas de match nul
		{
			System.out.println("Il y a match nul au bout de "+ cptRound + " rounds !");
		}
		else if (pikaLegende.isAlive()) //Si le premier Pokemon est toujours vivant, il gagne...
		{
			System.out.println(pikaLegende.getNom() + " a gagné en "+ cptRound + " rounds !");
		}
		else //...sinon c'est l'autre Pokemon qui est vivant et c'est celui-ci qui remporte la victoire
		{
			System.out.println(pikaMegaBrutal.getNom() + " a gagné en "+ cptRound + " rounds !");
		}
		
		System.out.println("======================================");

		
	}
}
