package tp4;

import java.util.Scanner;

public class CombatAvecJoueur
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Saisir le nom du premier combattant: ");
		Pokemon pokeJ1 = new Pokemon(scan.nextLine());
		System.out.println("Saisir le nom du second combattant: ");
		Pokemon pokeJ2 = new Pokemon(scan.nextLine());
		
		pokeJ1.sePresenter();
		pokeJ2.sePresenter();
		
		int cptRound = 0; //Compteur de round
		
		//On ne sort pas de la boucle tant que les Pokemonq ont la force de se battre
		while (pokeJ1.isAlive() && pokeJ2.isAlive())
		{
			System.out.println("=====================");
			
			cptRound++;
			System.out.println("Round "+cptRound);
			
			//Affichage situation
			System.out.println("Etat des combattants: " + pokeJ1.getNom() +": (en)"+ pokeJ1.getEnergie() + " (atk) "+pokeJ1.getPuissance()
			+" "+ pokeJ2.getNom() + ": (en)"+ pokeJ2.getEnergie() + " (atk) " + pokeJ2.getPuissance());
			
			//Choix J1
			System.out.println(pokeJ1.getNom() + ": Attaquer(0) ou Manger(1) ?");
			int choixJ1 = scan.nextInt();
			
				if (choixJ1 == 0) {
					pokeJ1.attaquer(pokeJ2);
				}
				else if (choixJ1 == 1) {
					pokeJ1.manger();
				}
				else {
					System.out.println( pokeJ1.getNom() + " ne fait rien ...");
				}
			
			//Choix J1
			System.out.println(pokeJ2.getNom() + ": Attaquer(0) ou Manger(1) ?");
			int choixJ2 = scan.nextInt();

				if (choixJ2 == 0) {
					pokeJ2.attaquer(pokeJ1);
				}
				else if (choixJ2 == 1) {
					pokeJ2.manger();
				}
				else {
					System.out.println(pokeJ2.getNom() + " ne fait rien ...");
				}	
		}
		System.out.println("=====================");

		
		//Affichage du vainqueur
		if( (pokeJ1.isAlive() || pokeJ2.isAlive()) == false) // Cas de match nul
		{
			System.out.println("Il y a match nul au bout de "+ cptRound + " rounds !");
		}
		else if (pokeJ1.isAlive()) //Si le premier Pokemon est toujours vivant, il gagne...
		{
			System.out.println(pokeJ1.getNom() + " a gagné en "+ cptRound + " rounds !");
		}
		else //...sinon c'est l'autre Pokemon qui est vivant et c'est celui-ci qui remporte la victoire
		{
			System.out.println(pokeJ2.getNom() + " a gagné en "+ cptRound + " rounds !");
		}
	}

}
