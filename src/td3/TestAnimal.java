package td3;

public class TestAnimal
{
	public static void main(String[] args)
	{
		Chien chien = new Chien();
		Animal animal = new Animal();
		Chat chat = new Chat();
		
		System.out.println(chat.affiche());
		
		System.out.println(animal.cri());
		System.out.println(chien.cri());
		System.out.println(chat.cri());
		System.out.println(chat.miauler());
		System.out.println(animal.miauler());
		
		/*
		 * On obtient une erreur au niveau de la ligne qui affiche
		 * le résultat de animal.miauler().
		 * L'erreur est dûe au fait que la méthode miauler()
		 * est spécifique à la classe Chat.
		 * En effet, l'héritage n'est pas à double sens: Tous les chat miaulent
		 * mais tous les animaux ne miaulent pas (seuls les chats le font).
		 */

	}
}
