package td3;

/*
Ici la classe Animal est fonctionnelle sans constructeur
car le constructeur par défaut sera créé automatiquement si on essaie de créer un nouvel objet Animal.
Dans cet état, on considère Animal comme une classe abstraite : Elle englobe tous les types d'animaux
donc on ne peut pas lui créer d'attributs en sachant que tous les animaux sont différents.
Animal sert donc de base à partir de laquelle on pourra créer des classes filles telles que Mammifère ou bien Oiseau, etc...
*/
public class Animal
{
	//Attributs
	
	//Constructeurs
	
	//Accesseurs
	
	//Méthodes
	
	public String affiche()
	{
		return "Je suis un animal";
	}
	
	public String cri()
	{
		return "...";
	}
	
	 public final String origine()
	 {
		 return "La classe Animal est la mère de toutes les classes !";
	 }
}
