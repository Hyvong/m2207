package td3;

public class Chien extends Animal
{
	//Attributs
	
	//Constructeurs
	
	//Accesseurs
	
	//Méthodes
	public String affiche()
	{
		return "Je suis un chien";
	}
	
	public String cri()
	{
		return "Ouaf ! Ouaf !";
	}
	
	/*
	public String origine()
	{
		return "La classe Chien est la meilleure des classes !";
	}
	*/
	
	//Ici le code ne peut pas être éxécuté car il est précisé dans la méthode origine() d'Animal qu'elle est 'final'.
	//Ainsi, origine() ne peut pas être redéfinie et par conséquent la méthode origine() de Chien ne fonctionne pas.
}
