package tp3;

public class Dame extends Humain
{
	//Attributs
	private boolean libre;
	
	//Constructeurs
	public Dame(String nom)
	{
		super(nom);
		this.boisson = "Martini";
		this.libre = true;
	}
	
	//Accesseurs
	public String quelEstTonNom() { return "Miss "+this.nom; }
	
	//Méthodes
	public void priseEnOtage()
	{
		this.libre = false;
		parler("Au secours !");
	}
	
	public void estLiberee()
	{
		this.libre = true;
		parler("Merci Cowboy.");
	}
	
	public void sePresenter()
	{
		super.sePresenter();
		
		if(this.libre)
		{
			parler("Actuellement, je suis libre.");
		}
		else
		{
			parler("Actuellement, je suis kidnappée.");
		}
	}
}
