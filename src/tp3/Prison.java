package tp3;

import java.util.Vector;

public class Prison
{
	//Attributs
	private String nom;
	private Vector tabBrigands;
	private int nbPrisonniers;
	
	//Constructeurs
	public Prison(String n)
	{
		this.nom = n;
		this.tabBrigands = new Vector(0);
		this.nbPrisonniers = 0;
	}
	
	//Accesseurs
	public String getNom() { return this.nom; }
	
	//Méthodes
	 public void mettreEnCellule(Brigand b)
	 {
		 tabBrigands.addElement(b);		 
		 
		 System.out.println(b.quelEstTonNom() + " a été mis dans la cellule n°" + this.nbPrisonniers+". ");
		 nbPrisonniers++;

	 }
	 
	 public void sortirDeCellule(Brigand b)
	 {	 
		 int indice = tabBrigands.indexOf(b);
		 tabBrigands.remove(indice);
		 
		 System.out.println(b.quelEstTonNom() + " est sorti de sa cellule.");
		 nbPrisonniers--; 
	 }
	 
	 void compterLesPrisonniers()
	 {
		 System.out.println("Il y a "+ this.tabBrigands.size()+ " brigand(s) dans "+ this.getNom());
	 }
}
