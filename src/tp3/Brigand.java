package tp3;

public class Brigand extends Humain
{
	//Attributs
	private String look;
	private int nbDame, recompense;
	private boolean enPrison;
	
	//Constructeurs
	public Brigand(String nom)
	{
		super(nom);
		
		this.look = "méchant";
		this.nbDame = 0;
		this.recompense = 100;
		this.enPrison = false;
		this.boisson = "cognac";
	}
	
	//Accesseurs
	public int getRecompense() { return this.recompense; }
	public String quelEstTonNom() { return this.nom+" le "+this.look; }
	
	//Méthodes
	public void sePresenter()
	{
		super.sePresenter();
		parler("J'ai l'air "+this.look+" et j'ai enlevé "+this.nbDame+" dames.");
		parler("Ma tête est mise à prix à "+this.recompense+"$ !!");
	}
	
	void enleve(Dame dame)
	{
		parler("Ah ah ! "+dame.quelEstTonNom()+", tu es ma prisonnière !");
		
		dame.priseEnOtage();
		this.nbDame++;
		this.recompense+=100;
	}
	
	void emprisonner(Sherif s)
	{
		parler("Damned, je suis fait ! "+s.quelEstTonNom()+", tu m'as eu !");
	}
}
