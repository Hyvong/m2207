package tp3;

public class Cowboy extends Humain
{
	//Attributs
	private int popularite;
	private String caracteristique;

	//Constructeurs
	public Cowboy(String nom)
	{
		super(nom);
		
		this.boisson = "whiskey";
		this.popularite = 0;
		this.caracteristique = "vaillant";
	}
	
	//Accesseurs
	public int getPopularite() { return this.popularite; }
	public String getCaracteristique() { return this.caracteristique; }
	
	//Méthodes
	public void tire(Brigand brigand)
	{
		System.out.println("Le "+this.caracteristique+" "+quelEstTonNom()+" tire sur "+brigand.quelEstTonNom()+ ". PAN !");
		parler("Prends ça, voyou !");
	}
	
	public void libere(Dame dame)
	{
		dame.estLiberee();
		this.popularite+=10;
	}
}
