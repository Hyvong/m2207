package tp3;

public class Histoire
{
	public static void main(String[] args)
	{
		Humain h1 = new Humain("Jean");
		
		h1.sePresenter();
		h1.boire();
		
		Dame dame1 = new Dame("Rebecca");
		
		dame1.sePresenter();
		dame1.priseEnOtage();
		dame1.estLiberee();
		
		Brigand brig1 = new Brigand("Mathias");
		
		brig1.sePresenter();
		
		Cowboy cow1 = new Cowboy("Fred");
		
		cow1.sePresenter();
		
		// 5.3) La dame et le brigand se présentent différement du cowboy -> Mathias le méchant et Miss Rebecca
		// Le nom en début de ligne ne change pas (voir commit 'Fix méthode parler()' ).
		
		System.out.println("================ SCENARIO EXERCICE 7.5 ===============");
		
		dame1.sePresenter();
		brig1.sePresenter();
		
		brig1.enleve(dame1);
		
		brig1.sePresenter();
		dame1.sePresenter();
		
		cow1.tire(brig1);
		cow1.libere(dame1);
		
		dame1.sePresenter();
		
		System.out.println("======================= FIN ==========================");
		
		Sherif sherif1 = new Sherif("Marley");
		
		sherif1.sePresenter();
		sherif1.coffrer(brig1);
		
		Cowboy clint = new Sherif("Clint");
		clint.sePresenter(); 
		
		//Exercice 9: On peut utiliser toutes les méthodes de Cowboy et seulement la méthode sePresenter de Sherif
		//Le Cowboy ne peut pas coffrer de Brigand car ce n'est pas une méthode qui est aussi définie dans la classe mère de Cowboy contrairement à sePresenter
		
		Prison prison1 = new Prison("Alcatraz");
		
		prison1.compterLesPrisonniers();
		prison1.mettreEnCellule(brig1);
		prison1.compterLesPrisonniers();
		prison1.sortirDeCellule(brig1);
		prison1.compterLesPrisonniers();
		
		Brigand brig2 = new Brigand("Fredo");
		Brigand brig3 = new Brigand("Rodolf");
		
		prison1.mettreEnCellule(brig2);
		prison1.mettreEnCellule(brig3);
		prison1.mettreEnCellule(brig1);
		prison1.compterLesPrisonniers();
		prison1.sortirDeCellule(brig2);
		prison1.compterLesPrisonniers();
		
		Cachot cachot1 = new Cachot("Le Grand Cachot");
		
		cachot1.mettreAuCachot(clint);
		cachot1.mettreAuCachot(brig2);
		cachot1.mettreAuCachot(dame1);
		cachot1.compterLesPrisonniers();
	}
}
