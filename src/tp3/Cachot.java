package tp3;

public class Cachot
{
	//Attributs
	private String nom;
	private Humain tabPrisonniers[];
	private int nbPrisonniers;
	
	//Constructeurs
	public Cachot(String n)
	{
		this.nom = n;
		this.tabPrisonniers = new Humain[10];
		for(int i =0; i<tabPrisonniers.length; i++)
		{
			tabPrisonniers[i]=null;
		}
		this.nbPrisonniers = 0;
	}
	
	//Accesseurs
	public String getNom() { return this.nom; }
	
	//Méthodes
	 public void mettreAuCachot(Humain h)
	 {
		 tabPrisonniers[nbPrisonniers] = h;		 
		 
		 System.out.println(h.quelEstTonNom() + " a été mis dans le cachot n°" + this.nbPrisonniers+". ");
		 nbPrisonniers++;

	 }
	 
	 public void sortirDuCachot(Humain h)
	 {	 
		 for (int i = 0; i < tabPrisonniers.length; i++)
		 {
			if (tabPrisonniers[i] == h)
			{
				tabPrisonniers[i] = null; 
			}
		 }
		 System.out.println(h.quelEstTonNom() + " est sorti de son cachot.");
		 nbPrisonniers--; 
	 }
	 
	 void compterLesPrisonniers()
	 {
		 int cpt=0;
		 for(int i = 0; i<this.tabPrisonniers.length; i++)
		 {
			 if (tabPrisonniers[i] != null)
			 {
				cpt++;
			 }
		 }
		 System.out.println("Il y a "+ cpt + " prisonnier(s) dans "+ this.getNom());
	 }

}
