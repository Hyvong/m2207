package tp3;

public class Sherif extends Cowboy
{
	//Attributs
	private int nbBrigands;
	
	//Constructeurs
	public Sherif(String nom)
	{
		super(nom);
		
		this.nbBrigands=0;
	}
	
	//Accesseurs
	public String quelEstTonNom() { return "Sherif "+this.nom; }
	public int getNombreBrigands() { return this.nbBrigands; }
	
	//Méthodes
	public void sePresenter()
	{
		super.sePresenter();
		parler("Je suis "+getCaracteristique()+" et ma popularité est de "+getPopularite()+".");
		parler("Durant ma carrière, j'ai arrêté "+ getNombreBrigands()+ " brigand(s).");
	}
	
	public void coffrer(Brigand b)
	{
		parler("Au nom de la loi, je vous arrête, "+b.quelEstTonNom()+" !");
		b.emprisonner(this);
	}
}
