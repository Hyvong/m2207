package tp3;

public class Humain
{
	//Attributs
	protected String nom, boisson;
	
	//Constructeurs
	public Humain(String nom)
	{
		this.nom = nom;
		this.boisson = "lait";
	}

	//Accesseurs
	public String quelEstTonNom()	{ return this.nom; }
	public String quelleEstTaBoisson() { return this.boisson; }
	
	//Méthodes
	public void parler(String texte)
	{
		//Permet d'éviter d'écrire un System.out.println() à chaque fois qu'on veut afficher dans la console.
		System.out.println("("+this.nom+") - "+texte);
	}
	
	public void sePresenter()
	{
		parler("Bonjour, je suis "+quelEstTonNom()+" et ma boisson préférée est le "+quelleEstTaBoisson()+".");
	}
	
	public void boire()
	{
		parler("Ah ! Un bon verre de "+quelleEstTaBoisson()+" ! GLOUPS !");
	}

}


