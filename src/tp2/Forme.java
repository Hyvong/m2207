package tp2;

public class Forme
{
	//Attributs
	private String couleur;
	private boolean coloriage;
	private static int compteurForme = 0;
	
	//Constructeurs
	public Forme()
	{
		this.couleur = "Orange";
		this.coloriage = true;
		compteurForme++;
	}
	
	public Forme(String c, boolean r)
	{
		this.couleur = c;
		this.coloriage = r;
		compteurForme++;
	}
	
	//Accesseurs
	public String getCouleur() { return this.couleur; }
	public boolean isColoriage() { return this.coloriage; }
	public void setCouleur(String c) { this.couleur = c; }
	public void setColoriage(boolean b) { this.coloriage = b; }
	public static int getCpt() { return compteurForme; }
	
	//Méthodes
	String seDecrire()
	{
		return "une Forme de couleur "+this.getCouleur()+" et de coloriage "+this.isColoriage()+". ";
	}
}
