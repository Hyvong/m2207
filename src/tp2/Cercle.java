package tp2;

public class Cercle extends Forme
{
	//Attributs
	private double rayon;
	
	
	//Constructeurs
	public Cercle()
	{
		super();
		this.rayon = 1.0;
	}
	
	public Cercle(double r)
	{
		super();
		this.rayon = r;
	}
	
	public Cercle(double r, String couleur, boolean coloriage)
	{
		super(couleur, coloriage);
		this.rayon = r;
	}
	
	//Accesseurs
	public double getRayon() { return this.rayon; }
	public void setRayon(double r) { this.rayon = r; }
	
	//Méthodes
	public String seDecrire()
	{
		return "un cercle de rayon " + this.getRayon() + " est issu d'" + super.seDecrire();
	}
	
	public double calculerAire()
	{
		return Math.PI*Math.pow(this.rayon, 2);
	}
	
	public double calculerPerimetre()
	{
		return Math.PI*2*this.rayon;
	}
}
