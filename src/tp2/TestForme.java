package tp2;

public class TestForme
{
	public static void main(String[] args)
	{
		System.out.println("Nombre de formes : "+ Forme.getCpt());
		
		Cylindre cy1 = new Cylindre();
		Cylindre cy2 = new Cylindre(4.2, 1.3, "bleu", true);
		Cercle c4 = new Cercle(3.2, "Rose", false);
		
		System.out.println("cy1 : "+ cy1.seDecrire());
		System.out.println("cy2 : "+cy2.seDecrire());
		
		System.out.println("Volume cy1 : "+cy1.calculerVolume());
		System.out.println("Volume cy2 : "+cy2.calculerVolume());

		System.out.println("Nombre de formes : "+ Forme.getCpt());

	}
}
