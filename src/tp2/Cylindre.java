package tp2;

public class Cylindre extends Cercle
{
	//Attributs
	private double hauteur;
	
	//Constructeurs
	public Cylindre()
	{
		super();
		this.hauteur = 1.0;
	}
	
	public Cylindre(double h, double r, String couleur, boolean b)
	{
		super(r, couleur, b);
		this.hauteur = h;
	}
	
	//Accesseurs
	public double getHauteur() { return this.hauteur; }
	public void setHauteur(double h) { this.hauteur = h; }
	
	//Méthodes
	public String seDecrire()
	{
		return "un Cylindre de hauteur "+ this.getHauteur() + " est issu d'" + super.seDecrire();
	}
	
	public double calculerVolume()
	{
		return super.calculerAire()*this.hauteur;
	}
}

