package td2;

public class TestRectangle
{
	public static void main(String[] args)
	{
		Rectangle rect = new Rectangle(2, 1, 5, 3);
		
		System.out.println("Surface : "+rect.surface()+" (unités d'aire)");
		
		Point p1 = new Point(2,1);
		Point p2 = new Point(5,2);
		
		Rectangle rect2 = new Rectangle(p1, p2);
		System.out.println("Surface : "+rect2.surface()+" (unités d'aire)");
	}

}
