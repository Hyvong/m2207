package td2;


public class Point
{
	//Attributs
	private int x;
	private int y;
	static int compteur = 0;
	
	//Constructeurs
	public Point(int x, int y)
	{
		this.x=x;
		this.y=y;
		
		compteur++;
	}
	public Point()
	{
		this.x=0;
		this.y=0;
		
		compteur++;
	}
	
	//Accesseurs
	public int getX() {return x;}
	public int getY() {return y;}
	public void setX(int val) {x = val;}
	public void setY(int val) {y = val;}
	
	
	//Méthodes
	
	public static int getCpt() {return compteur;}

	
	public String affiche()
	{
		return "Les coordonnées sont x="+this.x+" et y="+this.y;
	}
	
	public void resetCoordonnees()
	{
		this.x=0;
		this.y=0;
	}
	
	public void deplacer(int a, int b)
	{
		this.x+=a;
		this.y+=b;
	}
	
	public double distance(Point pt)
	{
		//Calcul de la distance entre deux points d'un plan
		return Math.sqrt( Math.pow((pt.x-this.x),2) + Math.pow((pt.y-this.y),2));
	}
	
	public boolean equals(Point pt)
	{
		if ( (this.x == pt.x) && (this.y == pt.y))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}


