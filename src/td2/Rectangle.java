package td2;

public class Rectangle
{
	//Attributs
	private Point p1 = new Point();
	private Point p2 = new Point();
	
	//Constructeurs
	public Rectangle(int x1, int y1, int x2, int y2)
	{
		//Point inférieur gauche
		this.p1.setX(x1);
		this.p1.setY(y1);	
		//Point supérieur droit
		this.p2.setX(x2);
		this.p2.setY(y2);
	}
	
	public Rectangle(Point point1, Point point2)
	{
		this.p1.setX(point1.getX());
		this.p1.setY(point1.getY());
		
		this.p2.setX(point2.getX());
		this.p2.setY(point2.getY());
	}
	
	//Accesseurs
	
	//Méthodes
	public float surface()
	{
		//Calcul de l'aire du rectangle	
		float longueur = this.p2.getY()-this.p1.getY();
		float largeur = this.p2.getX()-this.p1.getX();

		float aire = longueur*largeur;
		
		return aire;
	}
	
}
