package td2;

public class TestPoint 
{
	public static void main(String[] args)
	{
		//Création d'un nouveau point
		Point p = new Point(2, 7);
		Point p2 = new Point();
		
		System.out.println("x: "+p.getX());
		System.out.println("y: "+p.getY());
		
		/*
		System.out.println("Accès aux valeurs des attributs de manière directe (sans accesseurs)");
		System.out.println("x : "+p.x);
		System.out.println("y : "+p.y);
		*/
		
		//En public, les valeurs sont lisibles.
		//En private, on obtient l'erreur : "The field Point.x is not visible"

		System.out.println(p.affiche());
		System.out.println(p2.affiche());
		
		//Réinitialisation des coordonnées de p
		p.resetCoordonnees();
		System.out.println("Après le reset de p: "+p.affiche());
		
		//Déplacement du point p2
		p2.deplacer(5, 3);
		System.out.println("Après le déplacement de p2: "+p2.affiche());
		
		//Création de p3 et p4
		Point p3 = new Point(3, 4);
		Point p4 = new Point(6, 0);
		
		double dist = p3.distance(p4);
		System.out.println("Distance entre p3 et p4: "+dist);
		
		//On affiche le nombres d'objets Point créés
		System.out.println("Compteur: "+Point.getCpt());
		
		Point p5, p6, p7;
		p5 = new Point (2,3);
		p6 = new Point (2,3);
		p7 = p5;
		
		/*
		if(p5 == p6) {
			System.out.println("p5 est égal à p6 ! "); //Ne fonctionne pas
		}
		if (p5 == p7) {
			System.out.println("p5 est égal à p7 !"); //Fonctionne
		}
		*/
		
		if(p5.equals(p6)) //Test d'égalité de valeur entre p5 et p6
		{
			System.out.println("p5 est égal (en valeur) à p6");
		}
	}
}
