package tp6;

public class TestExceptions
{
	public static void main(String[] args)
	{
		int x=2, y=0;
		
		//System.out.println(x/y);
		//System.out.println("Fin du programme");
		
		//1.1 Ici on effectue une division par 0.
		//Comme c'est une opération interdite, le programme plante directement à l'éxécution.
		//Par conséquent, on ne passe pas à la ligne suivante et on en voit pas le message "Fin du programme".
		
		try
		{
			System.out.println("y/x = " + y/x);
			System.out.println("x/y = " + x/y);
			System.out.println("Commande de fermeture du programme");
			//1.3
			//On remarque que la première commande s'éxécute normalement puis que la deuxième lève une exception
			//On a pas accès à la dernière ligne "Commande de fermeture..." car le catch a pris la relève
		}
		catch (Exception e)
		{
			System.out.println("Une exception a été capturée");
		}
		System.out.println("Fin du programme");
			
		//1.2
		//On n'a plus d'erreur déxécution. A la place, le message "Une exception a été capturée" s'affiche puis "Fin du programme"
		//On en déduit que le programme a détécté que la division par 0 générait une erreur et à la place d'un plantage, affiche le message alternatif du catch
	}

}
