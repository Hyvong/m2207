package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class PanneauClient extends JFrame implements ActionListener
{
	//Attributs
	JTextField saisie;
	JButton envoyer;
	
	Socket monSocket;
	PrintWriter monPrintWriter;

	//Constructeurs
	public PanneauClient()
	{
		super();
		this.setTitle("Client - Panneau d'affichage");
		this.setSize(250, 120);
		this.setLocation(800, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.saisie = new JTextField(20);
		this.envoyer = new JButton("Envoyer");
		
		Container pane = getContentPane();
		pane.setLayout(new FlowLayout());
		
		pane.add(saisie);
		pane.add(envoyer);
		
		this.envoyer.addActionListener(this);
		
		try {
			//Création et connexion d'un socket client au serveur localhost port 8888
			monSocket = new Socket("localhost", 8888);
			
		} catch (Exception e) {
			System.out.println("Erreur de connexion au serveur");
		}
		
		setVisible(true);

	}

	//Accesseurs
	
	//Méthodes
	public void emettre()
	{
		try {
			System.out.println("Envoi d'un message");
			String message = this.saisie.getText();

			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			monPrintWriter.println(message);
			monPrintWriter.flush();
			
			this.saisie.setText("");

		} catch (Exception e) {
			System.out.println("Erreur d'envoi de message");
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		emettre(); //A chaque clic sur le bouton, on envoye le message entré
	}


	//Méthode main
	public static void main(String[] args)
	{
		PanneauClient monPanneauClient = new PanneauClient();
	}

}
