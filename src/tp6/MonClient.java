package tp6;

import java.io.PrintWriter;
import java.net.Socket;

public class MonClient
{
	public static void main(String[] args)
	{
		Socket monSocket;
		PrintWriter monPrintWriter;
		
		try {
			//Création d'un socket client, affichage du détail puis fermeture
			monSocket = new Socket("localhost", 8888);
			System.out.println("Client: "+ monSocket);
			
			 //Formatage d'une chaîne de caractère en flux de données
			monPrintWriter = new PrintWriter(monSocket.getOutputStream()); //Le PrintWriter gère le flux sortant
			System.out.println("Envoi du message Hello World");
			monPrintWriter.println("Hello World"); //On demande à générer un flux de données équivalant à la chaine "Hello World"
			monPrintWriter.flush(); 
			
			//4.2 la méthode flush permet d'envoyer la donnée une fois qu'elle est formatée
			
			monSocket.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erreur d'envoi du message");
		}
		
		//4.1
		//Avec le programme MonServeur éteint, la création de monSocket à destination du localhost provoque une erreur.
		//Avec MonServeur démarré, on réussi à établir une connexion et on obtient le détail sur le socket.
		
	}

}
