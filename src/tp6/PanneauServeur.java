package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PanneauServeur extends JFrame implements ActionListener
{
	//Attributs
	JButton exit;
	JTextArea zone1;
	
	ServerSocket server;
	Socket client;
	BufferedReader buffer;
	
	//Constructeurs
	public PanneauServeur()
	{
		super();
		this.setTitle("Serveur - Panneau d'affichage");
		this.setSize(400, 300);
		this.setLocation(300, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.zone1 = new JTextArea("Le panneau est actif\n");
		this.exit = new JButton("Quitter");
		
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		pane.add(zone1);
		pane.add(exit, BorderLayout.SOUTH);
		
		this.exit.addActionListener(this);
		
		setVisible(true);
		
		try {
			server = new ServerSocket(8888);
			System.out.println("ServerSocket: " + server);
			
			zone1.append("Serveur démarré\n"); //Si on a pas d'erreur, le serveur est correctement démarré
			
			ecouter(); //Le serveur ecoute sur son port 8888
			
			server.close();//Fermeture après réception
			
		} catch (Exception e) {
			System.out.println("Erreur de création ServerSocket");
			e.printStackTrace();
		}
		


	}

	//Accesseurs
	
	//Méthodes
	public void ecouter() 
	{
		try {
			//Attente de connexion
			zone1.append("Serveur en attente de connexion : ");

			//Autorisation de connexion
			client = server.accept();
			zone1.append("Client connecté \n");

			buffer = new BufferedReader(new InputStreamReader(client.getInputStream()));

			//Lecture du message reçu
			String ligne;
			while ((ligne = buffer.readLine()) != null){
				zone1.append("Message reçu : " + ligne + "\n");
			}
		} catch (Exception e) {
			System.out.println("Erreur dans le traitement de la connexion");
			e.printStackTrace();
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		System.exit(-1);  //Si on a ppuie sur le bouton quitter, l'application se ferme
	}

	//Méthode main
	public static void main(String[] args)
	{
		PanneauServeur monPanneauServeur = new PanneauServeur();
	}

}
