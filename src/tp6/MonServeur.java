package tp6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class MonServeur
{
	public static void main(String[] args)
	{
		ServerSocket monServerSocket;
		Socket monSocketClient; //On déclare une socket client
		
		BufferedReader monBufferedReader;
		
		try	{
			monServerSocket = new ServerSocket(8888); //Ouvre un socket serveur sur le port 8888
			System.out.println("ServerSocket: " + monServerSocket);
			
			monSocketClient = monServerSocket.accept(); //on effectue la connexion entre les deux sockets
			System.out.println("Le client s'est connecté");
			//LE PROGRAMME NE S'ARRETE PAS TANT QUE LE CLIENT N'EST PAS CONNECTE !
			
			//Permet de capter les données brutes du client et les convertir en caractères lisibles pour le serveur
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));
			String message = monBufferedReader.readLine(); //Permet de rentrer le message voulu
			System.out.println("Message : " + message);
			
			//Juste après la connexion, les sockets se ferment
			monServerSocket.close();
			monSocketClient.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}

		

	}
}